﻿using MvcApplication35.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication35.Controllers
{

    public class ProofreadController : Controller
    {

        //

        // GET: /Proofread/

        uploadDBEntities1 db = new uploadDBEntities1();

        public ActionResult Index()
        {

            return View();

        }

        public ActionResult BusinessList()
        {

            return View(db.Businesses.ToList());

        }



        // Get: /FileUpload // Deatails

        public ActionResult BusinessDetails(int id)
        {

            return View(db.Businesses.Find(id));

        }

        // Get: /FileUpload // Edit

        public ActionResult BusinessEdit(int id)
        {

            return View(db.Businesses.Find(id));

        }

        // Post: /Business // Edit

        [HttpPost, ValidateAntiForgeryToken]

        public ActionResult BusinessEdit(Business t, HttpPostedFileBase file)
        {

            if (file != null)
            {

                var fileName = Path.GetFileName(file.FileName);

                var path = Path.Combine(Server.MapPath("~/Content/Upload/Business"),

                fileName);

                file.SaveAs(path);

                ModelState.Clear();

                ViewBag.Message = "File updated successfully";

                string sd = file.FileName.Substring(file.FileName.Length - 3);

                string p = string.Empty;

                p = Server.MapPath("~/Content/Upload/Business");

                file.SaveAs(p + file.FileName);

                using (db)
                {

                    t.file = file.FileName;

                    db.Entry(t).State = EntityState.Modified;

                    db.SaveChanges();

                }

            }

            return RedirectToAction("BusinessList");

        }

        // Get: /Business // Delete

        public ActionResult BusinessDelete(int id)
        {

            return View(db.Businesses.Find(id));

        }

        // Post: /Business // Delete

        [HttpPost, ActionName("BusinessDelete")]

        public ActionResult Businessdelete_conf(int id)
        {

            Business t = db.Businesses.Find(id);

            db.Businesses.Remove(t);

            db.SaveChanges();

            return RedirectToAction("BusinessList");

        }

        // Finance List

        public ActionResult FinanceList()
        {

            return View(db.Finances.ToList());

        }

        // Get: /Finances // Deatails

        public ActionResult FinanceDetails(int id)
        {

            return View(db.Finances.Find(id));

        }

        // Get / finance // edit

        public ActionResult FinanceEdit(int id)
        {

            return View(db.Finances.Find(id));

        }

        // Post: /Finance // Edit

        [HttpPost, ValidateAntiForgeryToken]

        public ActionResult FinanceEdit(Finance t, HttpPostedFileBase file)
        {

            if (file != null)
            {

                var fileName = Path.GetFileName(file.FileName);

                var path = Path.Combine(Server.MapPath("~/Content/Upload/Finance"),

                fileName);

                file.SaveAs(path);

                ModelState.Clear();

                ViewBag.Message = "File updated successfully";

                string sd = file.FileName.Substring(file.FileName.Length - 3);

                string p = string.Empty;

                p = Server.MapPath("~/Content/Upload/Finance");

                file.SaveAs(p + file.FileName);

                using (db)
                {

                    t.file = file.FileName;

                    db.Entry(t).State = EntityState.Modified;

                    db.SaveChanges();

                }

            }

            return RedirectToAction("FinanceList");

        }

        // Get: /Finance // Delete

        public ActionResult FinanceDelete(int id)
        {

            return View(db.Finances.Find(id));

        }

        // Post: /Finance // Delete

        [HttpPost, ActionName("FinanceDelete")]

        public ActionResult Financedelete_conf(int id)
        {

            Finance t = db.Finances.Find(id);

            db.Finances.Remove(t);

            db.SaveChanges();

            return RedirectToAction("FinanceList");

        }

        // News List

        public ActionResult NewsList()
        {

            return View(db.News.ToList());

        }

        // Get: /News // Details

        public ActionResult NewsDetails(int id)
        {

            return View(db.News.Find(id));

        }

        // Get / News // edit

        public ActionResult NewsEdit(int id)
        {

            return View(db.News.Find(id));

        }

        // Post: /News // Edit

        [HttpPost, ValidateAntiForgeryToken]

        public ActionResult NewsEdit(News t, HttpPostedFileBase file)
        {

            if (file != null)
            {

                var fileName = Path.GetFileName(file.FileName);

                var path = Path.Combine(Server.MapPath("~/Content/Upload/News"),

                fileName);

                file.SaveAs(path);

                ModelState.Clear();

                ViewBag.Message = "File updated successfully";

                string sd = file.FileName.Substring(file.FileName.Length - 3);

                string p = string.Empty;

                p = Server.MapPath("~/Content/Upload/News");

                file.SaveAs(p + file.FileName);

                using (db)
                {

                    t.file = file.FileName;

                    db.Entry(t).State = EntityState.Modified;

                    db.SaveChanges();

                }

            }

            return RedirectToAction("NewsList");

        }

        //News delete

        // Get: /News // Delete

        public ActionResult NewsDelete(int id)
        {

            return View(db.News.Find(id));

        }

        // Post: /News // Delete

        [HttpPost, ActionName("NewsDelete")]

        public ActionResult Newsdelete_conf(int id)
        {

            News t = db.News.Find(id);

            db.News.Remove(t);

            db.SaveChanges();

            return RedirectToAction("NewsList");

        }

    }

}
