﻿using MvcApplication35.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication35.Controllers
{
    public class uploadController : Controller
    {
        //
        // GET: /upload/
        uploadDBEntities1 db = new uploadDBEntities1();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Business()
        {
            ViewBag.BusinessCategory = new SelectList(db.BusinessSubcategories, "Id", "SubcategoryName");
            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Business(Business t, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    int MaxContentLength = 1024 * 1024 * 60; //60 MB
                    string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf", ".wmv", ".mp4", ".mp3", ".wav", "mpeg", ".wma" };

                    if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                    {
                        // ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                        ViewBag.Message = "Please file of type: " + string.Join(", ", AllowedFileExtensions);
                    }

                    else if (file.ContentLength > MaxContentLength)
                    {
                        return Content("Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                    }
                    else
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/Upload/Business"), fileName);


                        file.SaveAs(path);
                        ModelState.Clear();
                        ViewBag.Message = "File uploaded successfully";
                        string ds = file.FileName.Substring(file.FileName.Length - 3);
                        string p = string.Empty;
                        p = Server.MapPath("~/Content/Upload/Business");
                        file.SaveAs(p + file.FileName);

                        using (db)
                        {

                            t.file = file.FileName;
                            db.Businesses.Add(t);
                            db.SaveChanges();

                        }

                    }

                }
                else
                {
                    return Content("Upload Your file to Continue");
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
                throw;
            }

        }

        public ActionResult Finance()
        {
            ViewBag.FinanceCategory = new SelectList(db.FinanceSubcategories, "Id", "SubcategoryName");
            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Finance(Finance t, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    int MaxContentLength = 1024 * 1024 * 60; //60 MB
                    string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf", ".html", ".htm", ".wmv", ".mp4", ".mp3", ".wav" };

                    if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                    {
                        // ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                        ViewBag.Message = "Please file of type: " + string.Join(", ", AllowedFileExtensions);
                    }

                    else if (file.ContentLength > MaxContentLength)
                    {
                        return Content("Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                    }
                    else
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/Upload/Finance"), fileName);


                        file.SaveAs(path);
                        ModelState.Clear();
                        ViewBag.Message = "File uploaded successfully";
                        string ds = file.FileName.Substring(file.FileName.Length - 3);
                        string p = string.Empty;
                        p = Server.MapPath("~/Content/Upload/Finance");
                        file.SaveAs(p + file.FileName);

                        using (db)
                        {

                            t.file = file.FileName;
                            db.Finances.Add(t);
                            db.SaveChanges();

                        }

                    }

                }
                else
                {
                    return Content("Upload Your file to Continue");
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
                throw;
            }


        }

        // News Upload 

        public ActionResult News()
        {
            ViewBag.NewsCategory = new SelectList(db.NewsSubcategories, "Id", "SubcategoryName");
            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult News(News t, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    int MaxContentLength = 1024 * 1024 * 60; //60 MB
                    string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf", ".html", ".htm", ".wmv", ".mp4", ".mp3", ".wav" };

                    if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                    {
                        // ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                        ViewBag.Message = "Please file of type: " + string.Join(", ", AllowedFileExtensions);
                    }

                    else if (file.ContentLength > MaxContentLength)
                    {
                        return Content("Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                    }
                    else
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/Upload/News"), fileName);


                        file.SaveAs(path);
                        ModelState.Clear();
                        ViewBag.Message = "File uploaded successfully";
                        string ds = file.FileName.Substring(file.FileName.Length - 3);
                        string p = string.Empty;
                        p = Server.MapPath("~/Content/Upload/News");
                        file.SaveAs(p + file.FileName);

                        using (db)
                        {

                            t.file = file.FileName;
                            db.News.Add(t);
                            db.SaveChanges();

                        }

                    }

                }
                else
                {
                    return Content("Upload Your file to Continue");
                }
                return RedirectToAction("Index");
            }

            catch (Exception ex)
            {

                return View("Error", new HandleErrorInfo(ex, "Home", "Index"));
                throw;
            }

        }
    }
}
