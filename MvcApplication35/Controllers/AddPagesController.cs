﻿using MvcApplication35.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication35.Controllers
{
    public class AddPagesController : Controller
    {
        //
        // GET: /AddPages/
        uploadDBEntities1 db = new uploadDBEntities1();
        public ActionResult Technology()
        {
            return View(db.Businesses.Where(m => m.SubcategoryId == 1).ToList());
        }

        public ActionResult RealEstate()
        {
            return View(db.Businesses.Where(m => m.SubcategoryId == 2).ToList());
        }

        public ActionResult SmallBusiness()
        {
            return View(db.Businesses.Where(m => m.SubcategoryId == 3).ToList());
        }

        public ActionResult Contract()
        {
            return View(db.Businesses.Where(m => m.SubcategoryId == 4).ToList());
        }

        public ActionResult Sports()
        {
            return View(db.News.Where(m => m.SubcategoryId == 1).ToList());
        }
        public ActionResult Education()
        {
            return View(db.News.Where(m => m.SubcategoryId == 2).ToList());
        }

        public ActionResult Weather()
        {
            return View(db.News.Where(m => m.SubcategoryId == 3).ToList());
        }

        public ActionResult Politics()
        {
            return View(db.News.Where(m => m.SubcategoryId == 4).ToList());
        }

        public ActionResult Market()
        {
            return View(db.Finances.Where(m => m.SubcategoryId == 1).ToList());
        }

        public ActionResult Stocks()
        {
            return View(db.Finances.Where(m => m.SubcategoryId == 2).ToList());
        }
        public ActionResult Currency()
        {
            return View(db.Finances.Where(m => m.SubcategoryId == 3).ToList());
        }
        public ActionResult Economy()
        {
            return View(db.Finances.Where(m => m.SubcategoryId == 4).ToList());
        }
    }
}
